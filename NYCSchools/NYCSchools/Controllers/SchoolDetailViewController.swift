//
//  SchoolDetailViewController.swift
//  NYCSchools
//
//  Created by Eugene Mammie on 11/30/18.
//  Copyright © 2018 Eugene Mammie. All rights reserved.
//

import UIKit

class SchoolDetailViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var avgCriticalLabel: UILabel!
    @IBOutlet weak var avgMathLabel: UILabel!
    @IBOutlet weak var avgWritingLabel: UILabel!
    @IBOutlet weak var numOfParticipantsLabel: UILabel!
    var school : School? // Usually dont want VC directly having model object
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // For sake of Brevity and simplicity this VC wont need its own view model so ill insert values Directly
        titleLabel.text = "Name: \(school?.name ?? "No Data")"
        numOfParticipantsLabel.text = "Number of Participants: \(school?.numOfParticipantsSAT ?? "")"
        avgCriticalLabel.text = "Avg. Critical Thinking Scores : \(school?.critcalReadAvgScoresSAT ?? "")"
        avgWritingLabel.text = "Avg. Writing Scores : \(school?.writingAvgScoresSAT ?? "")"
        avgMathLabel.text = "Avg. Math Scores : \(school?.mathAvgScoresSAT ?? "")"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
