//
//  SchoolListViewControllerTests.swift
//  NYCSchoolsTests
//
//  Created by Eugene Mammie on 12/2/18.
//  Copyright © 2018 Eugene Mammie. All rights reserved.
//

import XCTest
@testable import NYCSchools

class SchoolListViewControllerTests: XCTestCase {
    var sut: SchoolListViewController!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "SchoolListViewController") as? SchoolListViewController
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
