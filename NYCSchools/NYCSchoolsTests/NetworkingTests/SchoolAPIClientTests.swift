//
//  SchoolAPIClientTests.swift
//  NYCSchoolsTests
//
//  Created by Eugene Mammie on 11/29/18.
//  Copyright © 2018 Eugene Mammie. All rights reserved.
//

import XCTest
@testable import NYCSchools

class SchoolAPIClientTests: XCTestCase {
    var sut: SchoolAPIClient!
    var mockSession  = MockURLSession()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        sut = NYCSchoolAPIClient(aSession: URLSession.shared)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testSchoolRetrieval(){
        let expectation = XCTestExpectation(description: "Waiting for School API")
        sut.retrieveSchools(){ schools in
                XCTAssertNotNil(schools)
                XCTAssert((schools?.count)! > 1)
                expectation.fulfill()
        }
        wait(for: [expectation], timeout: 3.5)
    }
}

extension SchoolAPIClientTests {
    
    class MockURLSession : URLSessionProtocol {
        
        typealias Handler = (Data? , URLResponse?, Error?) -> Void
        
        var completionHandler: Handler?
        var url: URL?
        
        func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
                self.url = url
                self.completionHandler = completionHandler
                return URLSession.shared.dataTask(with: url)
        }
    }
    
    class MockURLSessionDataTask : URLSessionDataTask {
        var resumeGotCalled = false
        
        override func resume() {
            resumeGotCalled = true
        }
    }
    
}
