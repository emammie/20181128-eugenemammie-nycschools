//
//  SchoolAPIClient.swift
//  NYCSchools
//
//  Created by Eugene Mammie on 11/29/18.
//  Copyright © 2018 Eugene Mammie. All rights reserved.
//

import Foundation

typealias DataTaskResult = (Data?, URLResponse?, Error?) -> Void

protocol SchoolAPIClient {
    func retrieveSchools(completion: @escaping ([School]?) -> ())
    func retrieveSATSchools(completion: @escaping ([School]?) -> ())
}

protocol URLSessionProtocol {
    func dataTask(with: URL, completionHandler: @escaping DataTaskResult) -> URLSessionDataTask
}

class NYCSchoolAPIClient : SchoolAPIClient {
    var session: URLSessionProtocol
    
    init(aSession: URLSessionProtocol = URLSession.shared) {
        session = aSession
    }
    
    func retrieveSchools(completion: @escaping ([School]?) -> ()) {
        let url = URL(string:"https://data.cityofnewyork.us/resource/97mf-9njv.json")
        session.dataTask(with: url!) { data, URLResponse, Error in
            if Error != nil {
                print(Error.debugDescription)
            }
            let decoder = JSONDecoder()
            let listOfSchools = try! decoder.decode([School].self, from: data!)
            DispatchQueue.main.async {
                completion(listOfSchools)
            }
        }.resume()
    }
    
    func retrieveSATSchools(completion: @escaping ([School]?) -> ()) {
        let url = URL(string: "https://data.cityofnewyork.us/resource/734v-jeq5.json")
        session.dataTask(with: url!) { data, URLResponse, Error in
            if Error != nil {
                print(Error.debugDescription)
            }
            let decoder = JSONDecoder()
            let listOfSchools = try! decoder.decode([School].self, from: data!)
            DispatchQueue.main.async {
                completion(listOfSchools)
            }
        }.resume()
    }
}

extension URLSession : URLSessionProtocol { }
