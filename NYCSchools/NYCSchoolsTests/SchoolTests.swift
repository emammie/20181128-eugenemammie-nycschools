//
//  SchoolTests.swift
//  NYCSchoolsTests
//
//  Created by Eugene Mammie on 11/27/18.
//  Copyright © 2018 Eugene Mammie. All rights reserved.
//

import XCTest
@testable import NYCSchools

class SchoolTests: XCTestCase {
    var sut: School!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testCreateSchoolName(){
       // sut = School(name: "High School of the Arts")
       // XCTAssertNotNil(sut.name)
       // XCTAssert(sut.name == "High School of the Arts")
    }
    
    func testCreateSchoolsFromJSON(){
        if let path = Bundle.main.path(forResource: "test", ofType: "json")
        {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path))
                let decoder = JSONDecoder()
                let listOfSchools = try! decoder.decode([School].self, from: jsonData)
                
                XCTAssertNotNil(listOfSchools)
                XCTAssert(listOfSchools.count > 1)
            }
            catch {
                print("Problem Parsing")
            }
        }
    }
}
