//
//  SchoolListViewController.swift
//  NYCSchools
//
//  Created by Eugene Mammie on 11/27/18.
//  Copyright © 2018 Eugene Mammie. All rights reserved.
//

import UIKit

protocol SchoolListViewControllerDelegate: class {
    func didSelect(indexPath: Int)
}

class SchoolListViewController: UIViewController {
    
    @IBOutlet weak var schoolTableView : UITableView!
    weak var delegate : SchoolListViewControllerDelegate?
    var viewModel : SchoolListViewModel = SchoolListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        schoolTableView.delegate = self
        schoolTableView.dataSource = self
        
        viewModel.delegate = self
        viewModel.refreshSchools()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // In a In Production app this functionality of loading new screens could be given to a Coordinator
        if segue.identifier == "ShowDetail" {
            //let selectedIndex = self.schoolTableView.indexPathForCell(sender as UITableViewCell)
            if let indexPath = schoolTableView.indexPathForSelectedRow{
                let selectedRow = indexPath.row
                let detailVC = segue.destination as! SchoolDetailViewController
                detailVC.school = viewModel.schoolDetailMethod(index: selectedRow)
            }
        }
    }
}

extension SchoolListViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfSchools()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = schoolTableView.dequeueReusableCell(withIdentifier: "SchoolCell", for: indexPath) as! SchoolTableViewCell
        
        let title = viewModel.schoolNames(index:indexPath.row)
        let id = viewModel.schoolDatabaseNumber(index: indexPath.row)
    
        cell.configureCell(title: title , id: id)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // given more time I would give this work to a coordinator instead making it this VC responsibility
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detailVC = storyboard.instantiateViewController(withIdentifier: "SchoolDetailViewController") as! SchoolDetailViewController
        let selectedRow = indexPath.row
        let selectedSchool = viewModel.schoolDetailMethod(index: selectedRow)
        detailVC.school = selectedSchool
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
}

extension SchoolListViewController: SchoolListViewModelDelegate {
    func schoolsChanged() {
        self.schoolTableView.reloadData()
    }
}
