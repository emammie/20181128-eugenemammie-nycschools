//
//  SchoolTableViewCell.swift
//  NYCSchools
//
//  Created by Eugene Mammie on 11/30/18.
//  Copyright © 2018 Eugene Mammie. All rights reserved.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var idLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(title: String, id: String){
        titleLabel.text = title
        idLabel.text = id
    }

}
