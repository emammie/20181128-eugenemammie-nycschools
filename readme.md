NYCSchools documentation
================
# Notes

-This is a simple example example of a TableView fetching a number of school and accessing data on their SAT score

## Things considered
Keeping it brief I used a simple use of a ViewModel object to display on the View Controller 

With more time These considerations

- Instead of Delegation for Bindings using a Reactive Framework
- Separate Coordinator to control creation of Detail View Controllers
- Prefetching on tableviews to create a smoother experience and less memory
- Extensive Unit Testing on components
- UI Tests for Intergration
