//
//  SchoolListViewModel.swift
//  NYCSchools
//
//  Created by Eugene Mammie on 11/27/18.
//  Copyright © 2018 Eugene Mammie. All rights reserved.
//

import Foundation

protocol SchoolListViewModelDelegate: class {
    func schoolsChanged()
}

class SchoolListViewModel {
   private var schools : [School] {
        didSet {
            delegate?.schoolsChanged()
            retrieveScores()
        }
    }
    var satSchools = [String:School]()
    
    var client : SchoolAPIClient
    weak var delegate: SchoolListViewModelDelegate? //Using for data binding in Production would use Reactive Programming
    
    init(Schools: [School] = [School](), Client: SchoolAPIClient = NYCSchoolAPIClient()) {
        schools = Schools
        client = Client
    }
    func refreshSchools(){
        //Duplicated Code for simplicity usually would refactor
        client.retrieveSchools { NYCschools in
            guard let listOfSchools = NYCschools else {
                self.noSchoolData()
                return
            }
            DispatchQueue.main.async {
                self.schools = listOfSchools
            }
        }
    }
    func retrieveScores(){
        //given more time these type of calls would scale better in the model Layer 
        client.retrieveSATSchools{ NYCschools in
            guard let listOfSchools = NYCschools else {
                self.noSchoolData()
                return
            }
            var SATschools = [String: School]()
            listOfSchools.map { school in
                SATschools[school.id] = school
            }
            DispatchQueue.main.async {
                self.satSchools = SATschools
            }
        }
    }
    func noSchoolData(){
        //In real Production APP can be used to add error messaging
        print("No School Data")
    }
}


extension SchoolListViewModel {
    
    func numberOfSchools() -> Int {
        return schools.count
    }
    
    func schoolNames(index : Int) -> String {
        return schools[index].name
    }
    
    func schoolDatabaseNumber(index : Int) -> String {
        return schools[index].id
    }
    
    func schoolDetailMethod(index : Int) -> School? {
        let school = schools[index]
        guard let aSchool =  satSchools[school.id]
            else { return nil }
        return aSchool
    }
}
